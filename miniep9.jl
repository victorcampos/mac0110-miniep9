function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
            c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    if p < 1
        return "Valor da potência inválido!"
    elseif p == 1
        return M
    else
        res = M
        for i in 1:(p-1)
            res = multiplica(M,res)
            i += 1
        end
    return res
    end
end

function matrix_pot_by_squaring(M, p)
    if p < 1
        return "Valor da potência inválido!"
    elseif p == 1
        return M
    else
        #o método consiste em "fatorar" o expoente para maior eficiência no algoritmo recursivo.
        if (iseven(p))
            return matrix_pot_by_squaring(multiplica(M,M), p÷2)
        elseif (!iseven(p))
            return multiplica(M, matrix_pot_by_squaring(multiplica(M,M),(p-1)÷2))
        end
    end
end