include("miniep9.jl")

using Test
function testaMiniEP9()
    println("Testando funções.... ")
    println("matrix_pot....     ")
    @test matrix_pot([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot([1 2; 3 4], 2) == [7.0 10.0; 15.0 22.0]
    @test isapprox(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7), [ 5.64284e8 4.70889e8 3.23583e8 3.51859e8; 
    8.31242e8 6.93529e8 4.76619e8 5.18192e8;
    5.77004e8 4.81473e8 3.30793e8 3.59677e8;
    7.99037e8 6.66708e8 4.58121e8 4.98127e8 ], atol = 2000.0)
    println("OK!")
    println("matrix_pot_by_squaring....   ")
    @test matrix_pot_by_squaring([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot_by_squaring([1 2; 3 4], 2) == [7.0 10.0; 15.0 22.0]
    @test isapprox(matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7), [ 5.64284e8 4.70889e8 3.23583e8 3.51859e8; 
    8.31242e8 6.93529e8 4.76619e8 5.18192e8;
    5.77004e8 4.81473e8 3.30793e8 3.59677e8;
    7.99037e8 6.66708e8 4.58121e8 4.98127e8 ], atol = 2000.0)
    println("OK!")
    println(" ")
    printstyled("Todos os testes obtiveram sucesso!", bold = true, color = :green)
end

testaMiniEP9()

