# MAC0110-MiniEP9

MiniEP9 coursework for MAC0110

This MiniEP9 consists of three files, miniep9.jl, testaMiniEP9.jl and resposta.jl.
miniep9.jl employes three functions, a multiplica(a, b) function for multiplying 
matrix arrays, a matrix_pot(a, n) function that utilizes the first one to calculate 
the value of a matrix to the power of n, and a matrix_pot_by_squaring(a, n) function
that does the same as the former but employing a 'exponentiation by squaring' recursive
algorithm that greatly diminishes the computational complexity and memory allocation
that's necessary for the function to achieve its task. 

USAGE: testaMiniEP9.jl contains automated tests for the given functions, whilst
the resposta.jl file compares the time complexity of the two function implementations
and answers the question about their respective performances.