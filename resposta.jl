include("miniep9.jl")
include("testaMiniEP9.jl")
using LinearAlgebra
function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    println(" ")
    println("Testando tempo das funções para matriz identidade de dimensao 30: ")
    println("Expoente 10: ")
    print("matrix_pot: ")
    @time matrix_pot(M, 10)
    println(" ")
    print("matrix_pot_by_squaring: ")
    @time matrix_pot_by_squaring(M, 10)
    println("Expoente 1000: ")
    print("matrix_pot: ")
    @time matrix_pot(M, 1000)
    println(" ")
    print("matrix_pot_by_squaring: ")
    @time matrix_pot_by_squaring(M, 1000)
end

compare_times()

# RESPOSTA: O algoritmo de exponenciação por quadrados nos apresenta resultados substancialmente superiores,
# alocando espaços muito menores na memória e executando em períodos muito menores.

# Isso ocorre pois, ao contrário do laço comum utilizado em matrix_pot, a função matrix_pot_by_squaring
# calcula apenas a mesma matriz elevada ao quadrado de cada vez e soma suas partes, enquanto a função matrix_pot
# vai multiplicar valores cada vez maiores e maiores, sendo assim ineficaz para cálculos de ponto flutuante
# envolvendo valores muito grandes.